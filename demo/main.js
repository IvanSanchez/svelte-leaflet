import Demo from "./demo.svelte";

const demo = new Demo({
	target: document.body,
});

export default demo;
