# svelte-leaflet

LeafletJS maps for the SvelteJS reactive javascript framework.


### Demo

There's a small live demo at https://svelte.dev/repl/64f1f65db1e14f79b5261a5766914b5a?version=3.23.2

### Usage

`svelte-leaflet` provides a set of Svelte components with a 1-to-1 correspondence to Leaflet classes. The design goal is that properties passed to components should mimic parameters the the constructor of their Leaflet counterpart, with some of those being reactive wherever possible. As of now:

* `LeafletMap` ←→ `L.Map`
* `LeafletTileLayer` ←→ `L.TileLayer`
* `LeafletMarker` ←→ `L.Marker`
* `LeafletPopup` ←→ `L.Popup`

e.g.:

```
<script>
	import {
		LeafletMap,
		LeafletMarker,
		LeafletPopup,
		LeafletTileLayer,
	} from "svelte-leaflet";

	let markerVisible = true;
	let markerName = "Null Island";
	let markerLatLng = L.latLng([0, 0]);
</script>

<LeafletMap style="height:90vh; width: 80vw; position:absolute; left:20vw;">
	<LeafletTileLayer
		url={'https://a.tile.openstreetmap.org/{z}/{x}/{y}.png'}
		options={{ attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>' }} />
	{#if markerVisible}
		<LeafletMarker bind:latLng={markerLatLng} options={{ draggable: true }}>
			<LeafletPopup options={{ closeButton: false, closeOnClick: false }}>
				{markerName}
			</LeafletPopup>
		</LeafletMarker>
	{/if}
</LeafletMap>
```


